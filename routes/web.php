<?php

// welcome page
Route::get('/', 'IndexController@index');


// create race
Route::get('race/create', 'RaceController@create');
Route::post('race', 'RaceController@store');
Route::get('race/live', 'RaceController@live');
Route::post('race/info', 'RaceController@info');
Route::post('race/progress', 'RaceController@progress');
Route::get('race/top-records', 'RaceController@topRecords');