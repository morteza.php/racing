<?php

return [
    'max_reduced_speed' => 5,
    'reduced_speed_percentage' => 0.08,
    'default_horse_count_per_rice' => 8,
    'default_distance' => 1500,
    'max_race_count_at_the_same_time' => 3,
    'advance_on_each_step' => 10,
    'number_of_races_on_record_page' => 5,
    'number_of_horses_per_race_on_record_page' => 3,
];
