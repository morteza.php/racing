<?php

namespace App\Models;

use App\Helpers\NumberHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Horse extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function races()
    {
        return $this->belongsToMany(Race::class);
    }

    /**
     * @return Horse
     */
    public static function generateRandom()
    {
        $horse = new Horse();
        $horse->name = Str::random(rand(3, 16));
        $horse->base_speed = config('horse.default_base_speed');
        $horse->speed = NumberHelper::floatingRand(1, 10);
        $horse->strength = NumberHelper::floatingRand(1, 10);
        $horse->endurance = NumberHelper::floatingRand(1, 10);
        $horse->is_racing = true;

        return $horse;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function results()
    {
        return $this->hasMany(Result::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function lastResult()
    {
        return $this->hasOne(Result::class)->orderBy('iteration', 'DESC');
    }
}
