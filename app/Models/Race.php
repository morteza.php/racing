<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Race extends Model
{

    public static $STATUS_READY = 'ready';
    public static $STATUS_RACING = 'racing';
    public static $STATUS_FINISHED = 'finished';
    public static $STATUS_UNKNOWN = 'unknown';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function horses()
    {
        return $this->belongsToMany(Horse::class);
    }

    /**
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function results()
    {
        return $this->hasMany(Result::class);
    }
}
