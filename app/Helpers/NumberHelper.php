<?php

namespace App\Helpers;


class NumberHelper
{
    /**
     * @param int $min
     * @param int $max
     * @return float|int
     */
    public static function floatingRand(int $min, int $max)
    {
        return rand($min, ($max * 10)) / $max;
    }
}
