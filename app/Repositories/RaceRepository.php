<?php

namespace App\Repositories;

use App\Models\Race;
use App\Models\Result;
use Carbon\Carbon;

class RaceRepository
{
    /**
     * @return mixed
     */
    public static function getCountOfRunningRaces()
    {
        return Race::where('status', 'racing')->count();
    }

    /**
     * @param Race $race
     * @return mixed
     */
    public static function initRace(Race $race)
    {
        $results = [];


        foreach ($race->horses as $horse) {
            $iteration = 0;

            $results[] = [
                'horse_id' => $horse->id,
                'race_id' => $race->id,
                'iteration' => $iteration,
                'horse_speed' => $horse->base_speed + $horse->speed,
                'horse_distance_covered' => 0,
                'horse_position' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }

        if (Result::insert($results)) {
            $race->status = Race::$STATUS_RACING;
            return $race->save();
        }

        return false;
    }

    /**
     * @param Race $race
     * @return mixed
     */
    public static function runRace(Race $race)
    {
        $results = [];

        $steps = config('race.advance_on_each_step');

        foreach ($race->horses as $horse) {

            $lastResult = Result::where('race_id', $race->id)->where('horse_id', $horse->id)->orderBy('iteration', 'DESC')->firstOrFail();

            $horseDistanceCovered = $lastResult->horse_distance_covered;
            if ($horseDistanceCovered >= config('race.default_distance')) {
                continue;
            }

            $horseSpeedNormal = (float)number_format($horse->base_speed + $horse->speed, 2);
            $lastResultHorseSpeed = (float)number_format($lastResult->horse_speed, 2);
            $iteration = $lastResult->iteration;
            $horseSpeedReduced = (float)number_format(
                $horseSpeedNormal - (
                    config('race.max_reduced_speed') - $horse->strength * config('race.reduced_speed_percentage')
                )
            );


            for ($i = 0; $i < $steps; $i++) {

                ++$iteration;

                $horseSpeed = $lastResult->horse_speed;
                if ($horseSpeedNormal == $lastResultHorseSpeed) {
                    if (($horseSpeedNormal * $iteration) >= ($horse->endurance * 100)) {
                        $horseSpeed = $horseSpeedReduced;
                    }
                }

                // d = v * t
                $horseDistanceCovered += (float)number_format($horseSpeed * $iteration, 2);

                if (($i + 1) == $steps) {
                    $results[] = [
                        'horse_id' => $horse->id,
                        'race_id' => $race->id,
                        'iteration' => $iteration,
                        'horse_speed' => $horseSpeed,
                        'horse_distance_covered' => $horseDistanceCovered,
                        'horse_distance_covered_percentage' => (float)number_format((100 * $horseDistanceCovered) / $race->distance),
                        'horse_position' => 0,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
                }

            }
        }

        if (!count($results)) {
            $race->status = Race::$STATUS_FINISHED;
            $race->save();
            return true;
        }

        usort($results, function ($a, $b) {
            return $a['horse_distance_covered'] <=> $b['horse_distance_covered'];
        });

        $position = count($race->horses);
        foreach ($results as $key => $result) {
            $results[$key]['horse_position'] = $position;
            --$position;
        }

        return Result::insert($results);
    }
}
