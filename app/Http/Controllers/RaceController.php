<?php

namespace App\Http\Controllers;

use App\Models\Horse;
use App\Models\Race;
use App\Models\Result;
use App\Repositories\RaceRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RaceController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('race.create');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $maxRaceCountAtTheSameTime = config('race.max_race_count_at_the_same_time');
        if (RaceRepository::getCountOfRunningRaces() > $maxRaceCountAtTheSameTime) {
            return redirect('/race/create')->withErrors("Sorry, we cannot serve more than $maxRaceCountAtTheSameTime races at the same time.");
        }

        $horses = [];
        for ($i = 0; $i < config('race.default_horse_count_per_rice'); $i++) {
            $horses[] = Horse::generateRandom();
        }

        $race = New Race();
        $race->status = Race::$STATUS_READY;
        $race->distance = config('race.default_distance');
        $race->save();
        $race->horses()->saveMany($horses);

        if (!RaceRepository::initRace($race)) {
            return redirect('/race/create')->withErrors("Opps, something went wrong, we will take care of it soon...");
        }

        return redirect('/race/live');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function live()
    {
        return view('race.live');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function info(Request $request)
    {
        $raceId = (int)$request->get('raceId', 0);

        $races = Race::where('status', 'racing')
            ->with('horses.lastResult')
            ->orderBy('id', 'DESC')
            ->limit(config('race.max_race_count_at_the_same_time'));

        if ($raceId) {
            $races->where('id', $raceId);
        }
        $races = $races->get();

        return $races;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function progress(Request $request)
    {
        $raceId = (int)$request->get('raceId', 0);

        $races = Race::where('status', Race::$STATUS_RACING)
            ->with('horses.lastResult')
            ->orderBy('created_at', 'DESC')
            ->limit(config('race.max_race_count_at_the_same_time'));

        if ($raceId) {
            $races->where('id', $raceId);
        }
        $races = $races->get();

        foreach ($races as $race) {
            if (!RaceRepository::runRace($race)) {
                throw new \Exception('could not run the race!');
            }
        }

        $races = Race::where('status', 'racing')
            ->with('horses.lastResult')
            ->orderBy('id', 'DESC')
            ->limit(config('race.max_race_count_at_the_same_time'))
            ->get();

        return $races;
    }

    public function topRecords()
    {

        DB::enableQueryLog();


        $lastFiveRaces = Race::where('status', Race::$STATUS_FINISHED)
            ->orderBy('created_at', 'DESC')
            ->take(config('race.number_of_races_on_record_page'))
            ->get();


        $horseIds = [];
        $lastFiveResults = [];
        foreach ($lastFiveRaces as $lastFiveRace) {
            $lastFiveResultsSql = '
                SELECT horse_id, max(horse_position) as horsePosition
                FROM `results`
                WHERE `race_id` = ?
                GROUP BY horse_id
                ORDER BY horsePosition ASC
                LIMIT ?
                ;
               ';

            $results = DB::select($lastFiveResultsSql, [$lastFiveRace->id, config('race.number_of_horses_per_race_on_record_page')]);

            $lastFiveResults[$lastFiveRace->id] = $results;
        }


        foreach ($lastFiveResults as $lastFiveResult) {
            foreach ($lastFiveResult as $lastFiveRecord) {
                $horseIds[] = $lastFiveRecord->horse_id;
            }
        }
        $horseIds = array_unique($horseIds);

        $horses = Horse::whereIn('id', $horseIds)->get();
        dd('$horses', $horses);
    }
}


