@extends('parts.app')

@section('content')

    <div style="width: 80%;margin: 0 auto;margin-top: 40px;border: 1px solid #000;padding: 4px;">
        <form method="POST" action="{{ url('race') }}">
            @csrf
            <ul>
                <p>- I will create a race for you with 8 random horse.</p>
                <p>- Three races are allowed at the same time. (0 race(s) running now)</p>
            </ul>

            <button class="btn btn-success">Submit</button>
        </form>
    </div>

@endsection