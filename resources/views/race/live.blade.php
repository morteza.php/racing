@extends('parts.app')

@section('js')
    <script type="text/javascript">

        $(document).ready(function () {
            info();
        });

        function info(raceId) {

            var data = {
                _token: '{{ csrf_token() }}',
                raceId: raceId
            };

            var loadingSection = $('#loading');
            var liveRacesSection = $('#live_races');

            $.ajax({
                url: '{{ url('race/info') }}',
                type: 'POST',
                data: data,
                dataType: 'JSON',
                success: function (races) {

                    if (!races.length) {
                        loadingSection.html('No race at the moment!').show();
                        liveRacesSection.html('').hide();
                        return;
                    }

                    var racesHTML = '';
                    $(races).each(function (index, race) {
                        racesHTML += buildRace(race);
                    });

                    loadingSection.hide();
                    liveRacesSection.html(racesHTML).show();
                },
                error: function () {
                    alert('Opps, something went wrong!');
                }
            });
        }

        function buildRace(race) {
            var raceHTML = '<div class="race" id="race_' + race.id + '" style="width: 80%;margin: 0 auto;border: 1px solid #000;padding: 3px;"><h3>Race #' + race.id + '</h3><br>';
            $(race.horses).each(function (index, horse) {

                var horseInfo = 'ID: ' + horse.id + ', Name: ' + horse.name + ', Position: ' + horse.last_result.horse_position + ', Distance covered: ' + horse.last_result.horse_distance_covered + ', time: ' + horse.last_result.iteration;

                raceHTML += '<div class="progress">\n' +
                    '  <div class="progress-bar" role="progressbar" style="width: ' + horse.last_result.horse_distance_covered_percentage + '%;color:#000;" aria-valuenow="' + horse.last_result.horse_distance_covered_percentage + '" aria-valuemin="0" aria-valuemax="100">' + horseInfo + '</div>\n' +
                    '</div><br>';
            });

            raceHTML += getButton(race);

            raceHTML += '</div>';

            return raceHTML;
        }

        function getButton(race) {
            return '<br><div style="margin: 0 auto; text-align: center;">\n' +
                '            <button onclick="progress(' + race.id + ')" class="btn btn-primary center-block btn-sm">Progress</button>\n' +
                '        </div>';
        }

        function progress(raceId) {

            var data = {
                _token: '{{ csrf_token() }}',
                raceId: raceId
            };

            $.ajax({
                url: '{{ url('race/progress') }}',
                type: 'POST',
                data: data,
                dataType: 'JSON',
                success: function () {
                    info(raceId);
                },
                error: function () {
                    alert('Opps, something went wrong!');
                }
            });
        }
    </script>
@endsection

@section('content')
    <div id="loading">Loading...</div>
    <div id="live_races" style="display: none;">
    </div>
@endsection