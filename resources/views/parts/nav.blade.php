<?php
$segments = Request::segments();

// dd('$segments', $segments);

$activeMenu = 'home';
if (!empty($segments[0]) && !empty($segments[1]) && $segments[0] == 'race' && $segments[1] == 'create') {
    $activeMenu = 'raceCreate';
} elseif (!empty($segments[0]) && !empty($segments[1]) && $segments[0] == 'race' && $segments[1] == 'live') {
    $activeMenu = 'raceLive';
} elseif (!empty($segments[0]) && !empty($segments[1]) && $segments[0] == 'race' && $segments[1] == 'top-records') {
    $activeMenu = 'raceTopRecords';
}

?>
<div class="m-1">
    <ul class="nav nav-pills nav-fill">
        <li class="nav-item">
            <a class="nav-link {{ ($activeMenu=='home') ? 'active' : '' }}" href="{{ url('/') }}">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ ($activeMenu=='raceLive') ? 'active' : '' }}" href="{{ url('race/live') }}">Live races</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ ($activeMenu=='raceTopRecords') ? 'active' : '' }}" href="{{ url('race/top-records') }}">Records</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ ($activeMenu=='raceCreate') ? 'active' : '' }}" href="{{ url('race/create') }}">Create race</a>
        </li>
    </ul>
</div>
