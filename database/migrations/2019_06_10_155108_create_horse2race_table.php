<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorse2raceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horse_race', function (Blueprint $table) {

            $table->unsignedBigInteger('horse_id');
            $table->foreign('horse_id')->references('id')->on('horses');

            $table->unsignedBigInteger('race_id');
            $table->foreign('race_id')->references('id')->on('races');

            $table->primary(['horse_id', 'race_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horse_race');
    }
}
