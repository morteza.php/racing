<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {

            $table->unsignedBigInteger('horse_id');
            $table->foreign('horse_id')->references('id')->on('horses');

            $table->unsignedBigInteger('race_id');
            $table->foreign('race_id')->references('id')->on('races');

            $table->unsignedBigInteger('iteration')->default(0);

            $table->float('horse_speed', 4, 2)->default(0);
            $table->float('horse_distance_covered', 6, 2)->default(0);
            $table->float('horse_distance_covered_percentage', 5, 2)->default(0);
            $table->unsignedBigInteger('horse_position')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
