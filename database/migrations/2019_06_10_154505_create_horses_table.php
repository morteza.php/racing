<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('name', 255);
            $table->float('base_speed', 4, 2)->unsigned()->default(5);
            $table->float('speed', 4, 2)->unsigned()->default(0);
            $table->float('strength', 4, 2)->default(0);
            $table->float('endurance', 4, 2)->default(0);
            $table->boolean('is_racing')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horses');
    }
}
